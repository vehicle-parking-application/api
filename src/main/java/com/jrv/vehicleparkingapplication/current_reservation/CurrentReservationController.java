package com.jrv.vehicleparkingapplication.current_reservation;

import com.jrv.vehicleparkingapplication.dto.ParkingReservationDTO;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservation;
import com.jrv.vehicleparkingapplication.utils.Constants;
import com.jrv.vehicleparkingapplication.utils.Message;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = Constants.UI_LOCAL_BASE_URL, maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api/parking")
public class CurrentReservationController {

    private static final Logger logger = LoggerFactory.getLogger(CurrentReservationController.class);

    private final CurrentReservationService currentReservationService;

    private final ModelMapper modelMapper;

    @Autowired
    public CurrentReservationController(CurrentReservationService currentReservationService, ModelMapper modelMapper) {
        this.currentReservationService = currentReservationService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/current-reservation")
    public ResponseEntity<?> getCurrentReservation(@RequestParam Long userId) {
        ParkingReservationDTO currentReservationDto = new ParkingReservationDTO();
        try {
            Optional<ParkingReservation> currentReservation = currentReservationService.getCurrentReservation(userId);
            if (currentReservation.isPresent())
                currentReservationDto = modelMapper.map(currentReservation.get(), ParkingReservationDTO.class);
            else
                currentReservationDto = new ParkingReservationDTO();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
        return ResponseEntity.ok(currentReservationDto);
    }

    @GetMapping("/is-current-reservation-present")
    public Boolean isCurrentReservationPresent(@RequestParam Long userId) {
        Boolean isPresent = false;
        try {
            Optional<ParkingReservation> currentReservation = currentReservationService.getCurrentReservation(userId);
            isPresent = currentReservation.isPresent();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return isPresent;
    }

    @PostMapping("/add-one-hour")
    public ResponseEntity<?> addOneHour(@RequestBody ParkingReservationDTO currentReservationDto){
        ParkingReservationDTO modifiedCurrentReservationDto = new ParkingReservationDTO();
        try{
            ParkingReservation currentReservation = modelMapper.map(currentReservationDto, ParkingReservation.class);
            ParkingReservation modifiedParkingReservation = currentReservationService.addOneHour(currentReservation);
            modifiedCurrentReservationDto = modelMapper.map(modifiedParkingReservation, ParkingReservationDTO.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok(modifiedCurrentReservationDto);
    }

    @PostMapping("/leave")
    public ResponseEntity<?> leave(@RequestBody ParkingReservationDTO currentReservationDto){
        String statusMessage = null;
        try{
            ParkingReservation currentReservation = modelMapper.map(currentReservationDto, ParkingReservation.class);
            statusMessage = currentReservationService.leave(currentReservation);
        } catch (Exception e){
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok(new Message(statusMessage));
    }

    @PostMapping("/cancel")
    public ResponseEntity<?> cancel(@RequestBody ParkingReservationDTO currentReservationDto){
        String statusMessage = null;
        try{
            ParkingReservation currentReservation = modelMapper.map(currentReservationDto, ParkingReservation.class);
            statusMessage = currentReservationService.cancel(currentReservation);
        } catch (Exception e){
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok(new Message(statusMessage));
    }
}
