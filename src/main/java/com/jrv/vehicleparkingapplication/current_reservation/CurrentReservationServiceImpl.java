package com.jrv.vehicleparkingapplication.current_reservation;

import com.jrv.vehicleparkingapplication.reservation.ParkingReservation;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservationRepository;
import com.jrv.vehicleparkingapplication.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class CurrentReservationServiceImpl implements CurrentReservationService {

    private final ParkingReservationRepository reservationRepository;

    @Autowired
    public CurrentReservationServiceImpl(ParkingReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Optional<ParkingReservation> getCurrentReservation(Long userId) {
        LocalDateTime currentTime = LocalDateTime.now();
        return reservationRepository.findAll().stream()
                .filter(reservation -> reservation.getUser().getId().equals(userId))
                .filter(reservation -> reservation.getTimestamp().toLocalDate().isEqual(currentTime.toLocalDate()))
                .filter(reservation -> !Constants.LEFT_STATUS.equals(reservation.getStatus()))
                .filter(reservation -> !Constants.CANCELLED_STATUS.equals(reservation.getStatus()))
                .findFirst();
    }

    @Override
    public ParkingReservation addOneHour(ParkingReservation currentReservation) {
        currentReservation.setToTime(currentReservation.getToTime().plusHours(1L));
        return reservationRepository.save(currentReservation);
    }

    @Override
    public String leave(ParkingReservation currentReservation) {
        currentReservation.setStatus(Constants.LEFT_STATUS);
        reservationRepository.save(currentReservation);
        return Constants.LEAVE_SUCCESS_MESSAGE;
    }

    @Override
    public String cancel(ParkingReservation currentReservation) {
        currentReservation.setStatus(Constants.CANCELLED_STATUS);
        reservationRepository.save(currentReservation);
        return Constants.CANCELLED_SUCCESS_MESSAGE;    }
}
