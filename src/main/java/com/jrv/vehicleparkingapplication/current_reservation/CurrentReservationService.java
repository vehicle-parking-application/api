package com.jrv.vehicleparkingapplication.current_reservation;

import com.jrv.vehicleparkingapplication.reservation.ParkingReservation;

import java.util.Optional;

public interface CurrentReservationService {
    Optional<ParkingReservation> getCurrentReservation(Long userId);

    ParkingReservation addOneHour(ParkingReservation currentReservationDto);

    String leave(ParkingReservation currentReservation);

    String cancel(ParkingReservation currentReservation);
}
