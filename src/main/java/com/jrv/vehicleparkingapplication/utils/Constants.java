package com.jrv.vehicleparkingapplication.utils;

public class Constants {
    public static final String ERROR = "Error";
    public static final String INDIAN_TIMEZONE_ID = "Asia/Kolkata";
    public static final String NO_PARKING_SLOTS_AVAILABLE = "No Parking Slots are available!";
    public static final String PARKING_RESERVED_SUCCESSFULLY = "Parking slot reserved successfully!";

    public static final String USER_ALREADY_HAS_SLOT = "User already has a Slot Reserved";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String UI_LOCAL_BASE_URL = "http://localhost:4200";

    public static final String LEFT_STATUS = "LEFT";
    public static final String CANCELLED_STATUS = "CANCELLED";
    public static final String LEAVE_SUCCESS_MESSAGE = "Marked as Left";
    public static final String CANCELLED_SUCCESS_MESSAGE = "Reservation Cancelled !";

}
