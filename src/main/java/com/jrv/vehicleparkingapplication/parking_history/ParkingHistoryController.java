package com.jrv.vehicleparkingapplication.parking_history;

import com.jrv.vehicleparkingapplication.dto.ParkingReservationDTO;
import com.jrv.vehicleparkingapplication.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = Constants.UI_LOCAL_BASE_URL, maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api/parking/history")
public class ParkingHistoryController {

    private final ParkingHistoryService parkingHistoryService;

    private static final Logger logger = LoggerFactory.getLogger(ParkingHistoryController.class);

    @Autowired
    public ParkingHistoryController(ParkingHistoryService parkingHistoryService){
        this.parkingHistoryService = parkingHistoryService;
    }

    @GetMapping("/get-brief-history")
    public ResponseEntity<?> getBriefHistory(@RequestParam Long userId){
        List<ParkingReservationDTO> parkingReservationHistory = new ArrayList<>();
        try {
            parkingReservationHistory = parkingHistoryService.getBriefHistory(userId);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
        return ResponseEntity.ok(parkingReservationHistory);
    }

}
