package com.jrv.vehicleparkingapplication.parking_history;

import com.jrv.vehicleparkingapplication.dto.ParkingReservationDTO;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservation;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParkingHistoryServiceImpl implements ParkingHistoryService{

    private final ParkingReservationRepository parkingReservationRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public ParkingHistoryServiceImpl(ParkingReservationRepository parkingReservationRepository,
                                     ModelMapper modelMapper){
        this.parkingReservationRepository = parkingReservationRepository;
        this.modelMapper = modelMapper;
    }
    @Override
    public List<ParkingReservationDTO> getBriefHistory(Long userId) {
        List<ParkingReservation> parkingReservationList = parkingReservationRepository.findLatest10ByUserId(userId);
        return parkingReservationList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private ParkingReservationDTO convertToDto(ParkingReservation parkingReservation) {
        return modelMapper.map(parkingReservation, ParkingReservationDTO.class);
    }
}
