package com.jrv.vehicleparkingapplication.parking_history;

import com.jrv.vehicleparkingapplication.dto.ParkingReservationDTO;

import java.util.List;

public interface ParkingHistoryService {
    List<ParkingReservationDTO> getBriefHistory(Long userId);
}
