package com.jrv.vehicleparkingapplication.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingSlotDTO {
    private String slotId;
    private String type;

}
