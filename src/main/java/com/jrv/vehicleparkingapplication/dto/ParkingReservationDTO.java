package com.jrv.vehicleparkingapplication.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingReservationDTO {
    private Long id;
    private UserDTO user;
    private ParkingSlotDTO parkingSlot;
    private LocalDateTime fromTime;
    private LocalDateTime toTime;
    private LocalDateTime timestamp;
    private String status;

}
