package com.jrv.vehicleparkingapplication.reservation;

import com.jrv.vehicleparkingapplication.current_reservation.CurrentReservationService;
import com.jrv.vehicleparkingapplication.security.User;
import com.jrv.vehicleparkingapplication.security.UserRepository;
import com.jrv.vehicleparkingapplication.utils.Constants;
import com.jrv.vehicleparkingapplication.utils.Utils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ParkingReservationServiceImpl implements ParkingReservationService {

    private final UserRepository userRepository;
    private final ParkingReservationRepository parkingReservationRepository;
    private final ModelMapper modelMapper;
    private final ParkingSlotRepository parkingSlotRepository;
    private final CurrentReservationService currentReservationService;

    @Autowired
    public ParkingReservationServiceImpl(UserRepository userRepository,
                                         ParkingReservationRepository parkingReservationRepository,
                                         ModelMapper modelMapper,
                                         ParkingSlotRepository parkingSlotRepository,
                                         CurrentReservationService currentReservationService) {
        this.userRepository = userRepository;
        this.parkingReservationRepository = parkingReservationRepository;
        this.modelMapper = modelMapper;
        this.parkingSlotRepository = parkingSlotRepository;
        this.currentReservationService = currentReservationService;
    }

    @Override
    public ParkingReservationResponse reserveParkingSlot(ParkingReservationRequest request) {
        User user = userRepository.findById(request.getUserId())
                .orElseThrow(() -> new RuntimeException(Constants.USER_NOT_FOUND));
        if(currentReservationService.getCurrentReservation(user.getId()).isPresent()){
            throw new UserAlreadyHasReservationException(Constants.USER_ALREADY_HAS_SLOT);
        }
        LocalDateTime fromTime = Utils.convertUtcToTimezone(Utils.parseDateTime(request.getFromTime()));
        LocalDateTime toTime = Utils.convertUtcToTimezone(Utils.parseDateTime(request.getToTime()));

        ParkingSlot parkingSlot = findAvailableSlot(request.getMode(), fromTime, toTime)
                .orElseThrow(() -> new NoParkingSlotsAvailableException(Constants.NO_PARKING_SLOTS_AVAILABLE));

        ParkingReservation reservation = modelMapper.map(request, ParkingReservation.class);

        reservation.setUser(user);
        reservation.setParkingSlot(parkingSlot);
        reservation.setFromTime(fromTime);
        reservation.setToTime(toTime);
        reservation.setTimestamp(LocalDateTime.now());

        parkingReservationRepository.save(reservation);
        return new ParkingReservationResponse(Constants.PARKING_RESERVED_SUCCESSFULLY);
    }

    private Optional<ParkingSlot> findAvailableSlot(String slotType, LocalDateTime fromTime, LocalDateTime toTime) {
        List<ParkingReservation> currentReservations = parkingReservationRepository.findAll()
                .stream()
                .filter(reservation -> reservation.getToTime().isAfter(fromTime))
                .filter(reservation -> reservation.getFromTime().isBefore(toTime))
                .filter(reservation -> reservation.getParkingSlot().getType().equals(slotType))
                .filter(reservation -> !Constants.LEFT_STATUS.equals(reservation.getStatus()))
                .filter(reservation -> !Constants.CANCELLED_STATUS.equals(reservation.getStatus()))
                .toList();
        List<ParkingSlot> allSlots = parkingSlotRepository.findByType(slotType);
        allSlots.removeAll(currentReservations.stream().map(ParkingReservation::getParkingSlot).toList());
        return allSlots.stream().findAny();
    }

}

