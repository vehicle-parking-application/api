package com.jrv.vehicleparkingapplication.reservation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingReservationResponse {
    private String message;

}
