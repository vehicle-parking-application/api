package com.jrv.vehicleparkingapplication.reservation;

import com.jrv.vehicleparkingapplication.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = Constants.UI_LOCAL_BASE_URL, maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api/parking")
public class ParkingReservationController {

    private final ParkingReservationService parkingReservationService;

    @Autowired
    public ParkingReservationController(ParkingReservationService parkingReservationService) {
        this.parkingReservationService = parkingReservationService;
    }

    @PostMapping("/reserve")
    public ResponseEntity<?> reserveParkingSlot(@RequestBody ParkingReservationRequest request) {
        try {
            ParkingReservationResponse response = parkingReservationService.reserveParkingSlot(request);
            return ResponseEntity.ok(response);
        } catch (UserAlreadyHasReservationException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        } catch (NoParkingSlotsAvailableException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
