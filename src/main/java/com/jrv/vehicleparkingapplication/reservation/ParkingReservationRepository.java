package com.jrv.vehicleparkingapplication.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParkingReservationRepository extends JpaRepository<ParkingReservation, Long> {
    @Query(value = "SELECT * FROM parking_reservations WHERE user_id = :userId ORDER BY timestamp DESC LIMIT 8", nativeQuery = true)
    List<ParkingReservation> findLatest10ByUserId(@Param("userId") Long userId);
}
