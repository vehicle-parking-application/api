package com.jrv.vehicleparkingapplication.reservation;

public interface ParkingReservationService {
    ParkingReservationResponse reserveParkingSlot(ParkingReservationRequest request);
}
