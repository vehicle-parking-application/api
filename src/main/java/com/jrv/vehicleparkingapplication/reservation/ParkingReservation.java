package com.jrv.vehicleparkingapplication.reservation;

import com.jrv.vehicleparkingapplication.security.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "parking_reservations")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingReservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "slot_id", nullable = false)
    private ParkingSlot parkingSlot;

    private LocalDateTime fromTime;

    private LocalDateTime toTime;

    private LocalDateTime timestamp;

    private String status;

}
