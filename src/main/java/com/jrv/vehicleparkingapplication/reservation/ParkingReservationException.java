package com.jrv.vehicleparkingapplication.reservation;

public class ParkingReservationException extends RuntimeException {
    public ParkingReservationException(String message) {
        super(message);
    }
}

class UserAlreadyHasReservationException extends ParkingReservationException {
    public UserAlreadyHasReservationException(String message) {
        super(message);
    }
}

class NoParkingSlotsAvailableException extends ParkingReservationException {
    public NoParkingSlotsAvailableException(String message) {
        super(message);
    }
}
