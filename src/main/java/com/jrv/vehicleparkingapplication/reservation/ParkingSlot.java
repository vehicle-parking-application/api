package com.jrv.vehicleparkingapplication.reservation;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "parking_slots")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingSlot {
    @Id
    @Column(name = "slot_id")
    private String slotId;

    @Column(name = "type")
    private String type;

}
