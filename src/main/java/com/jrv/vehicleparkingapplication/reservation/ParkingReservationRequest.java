package com.jrv.vehicleparkingapplication.reservation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParkingReservationRequest {
    private Long userId;
    private String mode;
    private String fromTime;
    private String toTime;
    
}
