package com.jrv.vehicleparkingapplication.security;

import com.jrv.vehicleparkingapplication.security.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AuthControllerTest {

    private static final Map<ERole, String> ROLE_MAPPING = new EnumMap<>(Map.of(
            ERole.ROLE_USER, "user",
            ERole.ROLE_MODERATOR, "mod",
            ERole.ROLE_ADMIN, "admin"
    ));
    @InjectMocks
    private AuthController authController;
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private PasswordEncoder encoder;
    @Mock
    private JwtUtils jwtUtils;
    @Mock
    private AuthenticationManager authenticationManager;

    @Test
    void testAuthenticationSuccess() {
        MockitoAnnotations.openMocks(this);

        Authentication authentication = mock(Authentication.class);
        when(authenticationManager.authenticate(any())).thenReturn(authentication);

        UserDetailsImpl userDetails = new UserDetailsImpl(1L, "test_user", "test@example.com", "password", null);
        when(authentication.getPrincipal()).thenReturn(userDetails);

        when(jwtUtils.generateJwtCookie(any())).thenReturn(ResponseCookie.from("jwtCookie", "jwtToken").build());

        when(userRepository.existsByUsername("test_user")).thenReturn(false);
        when(userRepository.existsByEmail("test@example.com")).thenReturn(false);

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("test_user");
        loginRequest.setPassword("password");
        ResponseEntity<?> responseEntity = authController.authenticateUser(loginRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("jwtCookie=jwtToken", responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE));
        assertEquals("test_user", ((UserInfoResponse) Objects.requireNonNull(responseEntity.getBody())).getUsername());
    }

    @Test
    void testAuthenticationFailure() {
        MockitoAnnotations.openMocks(this);

        when(authenticationManager.authenticate(any())).thenThrow(new AuthenticationException("Invalid Credentials") {
        });

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("invalid_user");
        loginRequest.setPassword("invalid_password");
        ResponseEntity<?> responseEntity = authController.authenticateUser(loginRequest);

        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
        assert (Objects.requireNonNull(responseEntity.getBody()).toString().contains("Invalid Credentials"));
    }

    @ParameterizedTest
    @EnumSource(value = ERole.class, names = {"ROLE_USER", "ROLE_MODERATOR", "ROLE_ADMIN"})
    void testSignupSuccess(ERole role) {
        MockitoAnnotations.openMocks(this);

        when(userRepository.existsByUsername("test_user")).thenReturn(false);
        when(userRepository.existsByEmail("test@example.com")).thenReturn(false);
        when(roleRepository.findByName(role)).thenReturn(Optional.of(new Role(role)));

        when(encoder.encode("testPassword")).thenReturn("encodedPassword");

        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("test_user");
        signUpRequest.setEmail("test@example.com");
        signUpRequest.setPassword("testPassword");
        signUpRequest.setRole(Collections.singleton(ROLE_MAPPING.get(role)));

        ResponseEntity<?> responseEntity = authController.registerUser(signUpRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void testSignupFailure_UserExists() {
        MockitoAnnotations.openMocks(this);

        when(userRepository.existsByUsername("existing_user")).thenReturn(true);

        SignupRequest signUpRequest = new SignupRequest();
        signUpRequest.setUsername("existing_user");
        signUpRequest.setEmail("existing@example.com");
        signUpRequest.setPassword("existingPassword");
        ResponseEntity<?> responseEntity = authController.registerUser(signUpRequest);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    void testLogoutUser() {
        MockitoAnnotations.openMocks(this);

        ResponseCookie cookie = ResponseCookie.from("jwtCookie", "").maxAge(0).build();
        when(jwtUtils.getCleanJwtCookie()).thenReturn(cookie);

        ResponseEntity<?> responseEntity = authController.logoutUser();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("You've been signed out!", ((MessageResponse) Objects.requireNonNull(responseEntity.getBody())).getMessage());
        assertEquals(cookie.toString(), responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE));
    }
}



