package com.jrv.vehicleparkingapplication.security;

import com.jrv.vehicleparkingapplication.security.AuthEntryPointJwt;
import com.jrv.vehicleparkingapplication.security.AuthTokenFilter;
import com.jrv.vehicleparkingapplication.security.UserDetailsServiceImpl;
import com.jrv.vehicleparkingapplication.security.WebSecurityConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WebSecurityConfigTest {

    @Mock
    UserDetailsServiceImpl userDetailsService;

    @Mock
    AuthEntryPointJwt unauthorizedHandler;

    @Mock
    HttpSecurity httpSecurity;

    @Mock
    AuthTokenFilter authTokenFilter;

    @Mock
    DaoAuthenticationProvider daoAuthenticationProvider;

    @InjectMocks
    WebSecurityConfig webSecurityConfig;

    @Test
    void filterChainTest() throws Exception {
        when(httpSecurity.csrf(any())).thenReturn(httpSecurity);
        when(httpSecurity.exceptionHandling(any())).thenReturn(httpSecurity);
        when(httpSecurity.sessionManagement(any())).thenReturn(httpSecurity);
        when(httpSecurity.authorizeHttpRequests(any())).thenReturn(httpSecurity);
        when(httpSecurity.authenticationProvider(any())).thenReturn(httpSecurity);
        when(httpSecurity.addFilterBefore(any(), any())).thenReturn(httpSecurity);

        webSecurityConfig.filterChain(httpSecurity);

        verify(httpSecurity, times(1)).csrf(any());
        verify(httpSecurity, times(1)).exceptionHandling(any());
        verify(httpSecurity, times(1)).sessionManagement(any());
        verify(httpSecurity, times(1)).authorizeHttpRequests(any());
        verify(httpSecurity, times(1)).authenticationProvider(any());
        verify(httpSecurity, times(1)).addFilterBefore(any(), any());
    }

    @Test
    void authenticationManagerTest() throws Exception {
        AuthenticationConfiguration authConfiguration = mock(AuthenticationConfiguration.class);
        AuthenticationManager authenticationManager = mock(AuthenticationManager.class);
        when(authConfiguration.getAuthenticationManager()).thenReturn(authenticationManager);

        webSecurityConfig.authenticationManager(authConfiguration);

        verify(authConfiguration, times(1)).getAuthenticationManager();
    }

    @Test
    void authenticationJwtTokenFilterTest() {
        AuthTokenFilter authTokenFilter = webSecurityConfig.authenticationJwtTokenFilter();
        assertNotNull(authTokenFilter);
    }

    @Test
    void daoAuthenticationProviderTest() {
        DaoAuthenticationProvider daoAuthenticationProvider = webSecurityConfig.authenticationProvider();
        assertNotNull(daoAuthenticationProvider);
    }

    @Test
    void passwordEncoderTest() {
        PasswordEncoder passwordEncoder = webSecurityConfig.passwordEncoder();
        assertNotNull(passwordEncoder);
        assertTrue(passwordEncoder instanceof BCryptPasswordEncoder);
    }
}

