package com.jrv.vehicleparkingapplication.current_reservation;

import com.jrv.vehicleparkingapplication.reservation.ParkingReservation;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservationRepository;
import com.jrv.vehicleparkingapplication.reservation.ParkingSlot;
import com.jrv.vehicleparkingapplication.security.User;
import com.jrv.vehicleparkingapplication.utils.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CurrentReservationServiceImplTest {

    @Mock
    private ParkingReservationRepository reservationRepository;

    @InjectMocks
    private CurrentReservationServiceImpl currentReservationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    @Test
    void getCurrentReservation_WhenReservationExists_ShouldReturnReservation() {
        Long userId = 1L;
        LocalDateTime currentTime = LocalDateTime.now();
        ParkingReservation reservation = new ParkingReservation();
        reservation.setId(1L);
        reservation.setTimestamp(currentTime);
        reservation.setUser(constructUser(userId));

        when(reservationRepository.findAll()).thenReturn(Arrays.asList(reservation));

        Optional<ParkingReservation> result = currentReservationService.getCurrentReservation(userId);

        assertEquals(Optional.of(reservation), result);
    }

    @Test
    void getCurrentReservation_WhenReservationDoesNotExist_ShouldReturnEmptyOptional() {
        Long userId = 1L;
        LocalDateTime currentTime = LocalDateTime.now();
        ParkingReservation reservation = new ParkingReservation();
        reservation.setId(2L); // Different ID
        reservation.setTimestamp(currentTime);
        reservation.setUser(constructUser(userId));

        when(reservationRepository.findAll()).thenReturn(Arrays.asList(reservation));

        Optional<ParkingReservation> result = currentReservationService.getCurrentReservation(userId);

        assertNotNull(result);
    }

    @Test
    void getCurrentReservation_WhenMultipleReservationsExist_ShouldReturnFirstMatchingReservation() {
        Long userId = 1L;
        LocalDateTime currentTime = LocalDateTime.now();
        ParkingReservation reservation1 = new ParkingReservation();
        reservation1.setId(1L);
        reservation1.setTimestamp(currentTime);
        reservation1.setUser(constructUser(userId));

        ParkingReservation reservation2 = new ParkingReservation();
        reservation2.setId(2L);
        reservation2.setTimestamp(currentTime.minusDays(1));
        reservation2.setUser(constructUser(userId));

        ParkingReservation reservation3 = new ParkingReservation();
        reservation3.setId(3L);
        reservation3.setTimestamp(currentTime.plusDays(1)); // Future timestamp
        reservation3.setUser(constructUser(userId));

        when(reservationRepository.findAll()).thenReturn(Arrays.asList(reservation1, reservation2, reservation3));

        Optional<ParkingReservation> result = currentReservationService.getCurrentReservation(userId);

        assertEquals(Optional.of(reservation1), result);
    }

    @Test
    void addOneHour_ShouldReturnModifiedParkingReservation(){
        ParkingReservation parkingReservation = currentReservation();
        ParkingReservation modifiedParkingReservation = parkingReservation;
        modifiedParkingReservation.setToTime(parkingReservation.getToTime().plusHours(1L));

        when(reservationRepository.save(modifiedParkingReservation)).thenReturn(modifiedParkingReservation);

        assertEquals(currentReservationService.addOneHour(parkingReservation).getToTime().getHour(),
                modifiedParkingReservation.getToTime().getHour());
    }


    @Test
    void leave_ShouldChangeStatus(){
        ParkingReservation parkingReservation = currentReservation();
        ParkingReservation modifiedParkingReservation = parkingReservation;
        modifiedParkingReservation.setStatus(Constants.LEFT_STATUS);

        when(reservationRepository.save(parkingReservation)).thenReturn(modifiedParkingReservation);
        parkingReservation = reservationRepository.save(parkingReservation);
        assertEquals(currentReservationService.leave(parkingReservation), Constants.LEAVE_SUCCESS_MESSAGE);
        assertEquals(parkingReservation.getStatus(), Constants.LEFT_STATUS);
    }

    @Test
    void cancel_ShouldChangeStatus(){
        ParkingReservation parkingReservation = currentReservation();
        ParkingReservation modifiedParkingReservation = parkingReservation;
        modifiedParkingReservation.setStatus(Constants.CANCELLED_STATUS);

        when(reservationRepository.save(parkingReservation)).thenReturn(modifiedParkingReservation);
        parkingReservation = reservationRepository.save(parkingReservation);
        assertEquals(currentReservationService.cancel(parkingReservation), Constants.CANCELLED_SUCCESS_MESSAGE);
        assertEquals(parkingReservation.getStatus(), Constants.CANCELLED_STATUS);
    }

    private User constructUser(Long userId){
        User user = new User("name", "name@mail.com", "password");
        user.setId(userId);
        return user;
    }

    private ParkingReservation currentReservation() {
        return  new ParkingReservation(1L, new User(),
                new ParkingSlot(), LocalDateTime.now(), LocalDateTime.now().plusHours(2L),
                LocalDateTime.now(), null);
    }
}
