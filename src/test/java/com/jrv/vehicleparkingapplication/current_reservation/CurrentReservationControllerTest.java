package com.jrv.vehicleparkingapplication.current_reservation;

import com.jrv.vehicleparkingapplication.dto.ParkingReservationDTO;
import com.jrv.vehicleparkingapplication.dto.ParkingSlotDTO;
import com.jrv.vehicleparkingapplication.dto.UserDTO;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservation;
import com.jrv.vehicleparkingapplication.reservation.ParkingSlot;
import com.jrv.vehicleparkingapplication.security.User;
import com.jrv.vehicleparkingapplication.utils.Constants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CurrentReservationControllerTest {

    @Mock
    private CurrentReservationService currentReservationService;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private CurrentReservationController currentReservationController;

    @Test
    void getCurrentReservation_WhenReservationExists_ShouldReturnReservationDTO() {
        Long userId = 1L;
        ParkingReservation reservation = new ParkingReservation();
        ParkingReservationDTO reservationDTO = new ParkingReservationDTO();
        when(currentReservationService.getCurrentReservation(userId)).thenReturn(Optional.of(reservation));
        when(modelMapper.map(reservation, ParkingReservationDTO.class)).thenReturn(reservationDTO);

        ResponseEntity<?> responseEntity = currentReservationController.getCurrentReservation(userId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.getBody() instanceof ParkingReservationDTO);
        assertEquals(reservationDTO, responseEntity.getBody());
    }

    @Test
    void getCurrentReservation_WhenReservationDoesNotExist_ShouldReturnEmptyReservationDTO() {
        Long userId = 1L;
        when(currentReservationService.getCurrentReservation(userId)).thenReturn(Optional.empty());

        ResponseEntity<?> responseEntity = currentReservationController.getCurrentReservation(userId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.getBody() instanceof ParkingReservationDTO);
        assertNotNull(responseEntity.getBody());
    }

    @Test
    void getCurrentReservation_WhenExceptionThrown_ShouldReturnInternalServerError() {
        Long userId = 1L;
        when(currentReservationService.getCurrentReservation(userId)).thenThrow(new RuntimeException("Something went wrong"));

        ResponseEntity<?> responseEntity = currentReservationController.getCurrentReservation(userId);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
    }

    @Test
    void isCurrentReservationPresent_WhenReservationExists_ShouldReturnTrue() {
        Long userId = 1L;
        when(currentReservationService.getCurrentReservation(userId)).thenReturn(Optional.of(new ParkingReservation()));

        Boolean isPresent = currentReservationController.isCurrentReservationPresent(userId);

        assertTrue(isPresent);
    }

    @Test
    void isCurrentReservationPresent_WhenReservationDoesNotExist_ShouldReturnFalse() {
        Long userId = 1L;
        when(currentReservationService.getCurrentReservation(userId)).thenReturn(Optional.empty());

        Boolean isPresent = currentReservationController.isCurrentReservationPresent(userId);

        assertFalse(isPresent);
    }

    @Test
    void isCurrentReservationPresent_WhenExceptionThrown_ShouldReturnFalse() {
        Long userId = 1L;
        when(currentReservationService.getCurrentReservation(userId)).thenThrow(new RuntimeException("Something went wrong"));

        Boolean isPresent = currentReservationController.isCurrentReservationPresent(userId);

        assertFalse(isPresent);
    }

    @Test
    void addOneHour_WhenSuccess_ShouldReturnPlusOneHour() {
        ParkingReservationDTO currentReservationDTO = currentReservationDTO();
        ParkingReservation currentReservation = currentReservation();
        ParkingReservation modifiedParkingReservation = currentReservation;
        modifiedParkingReservation.setToTime(currentReservation.getToTime().plusHours(1L));
        ParkingReservationDTO modifiedParkingReservationDto = currentReservationDTO;
        modifiedParkingReservationDto.setToTime(currentReservationDTO.getToTime().plusHours(1L));

        when(currentReservationService.addOneHour(currentReservation)).thenReturn(modifiedParkingReservation);
        when(modelMapper.map(currentReservationDTO, ParkingReservation.class)).thenReturn(currentReservation);
        when(modelMapper.map(modifiedParkingReservation, ParkingReservationDTO.class)).thenReturn(currentReservationDTO);

        ResponseEntity<ParkingReservationDTO> responseEntity = (ResponseEntity<ParkingReservationDTO>) currentReservationController.addOneHour(currentReservationDTO);

        assertEquals(responseEntity.getBody().getToTime().getHour(), modifiedParkingReservation.getToTime().getHour());

    }

    @Test
    void addOneHour_WhenExceptionThrown_ShouldReturnInternalServerError() {
        ParkingReservationDTO currentReservationDTO = currentReservationDTO();
        ParkingReservation currentReservation = currentReservation();

        when(currentReservationService.addOneHour(currentReservation)).thenThrow(new RuntimeException());

        ResponseEntity<?> responseEntity = currentReservationController.addOneHour(currentReservationDTO);

        assertEquals(responseEntity.getStatusCode(), HttpStatusCode.valueOf(500));
    }

    @Test
    void leave_WhenSuccess_ShouldReturnSuccessMessage() {
        ParkingReservationDTO currentReservationDTO = currentReservationDTO();
        ParkingReservation currentReservation = currentReservation();

        when(modelMapper.map(currentReservationDTO, ParkingReservation.class)).thenReturn(currentReservation);
        when(currentReservationService.leave(currentReservation)).thenReturn(Constants.LEAVE_SUCCESS_MESSAGE);

        assertTrue(currentReservationController.leave(currentReservationDTO).getBody().toString()
                .contains(Constants.LEAVE_SUCCESS_MESSAGE));
    }

    @Test
    void leave_WhenExceptionThrown_ShouldReturnInternalServerError() {
        ParkingReservationDTO currentReservationDTO = currentReservationDTO();
        ParkingReservation currentReservation = currentReservation();

        when(modelMapper.map(currentReservationDTO, ParkingReservation.class)).thenReturn(currentReservation);
        when(currentReservationService.leave(currentReservation)).thenThrow(new RuntimeException());

        assertEquals(currentReservationController.leave(currentReservationDTO).getStatusCode(),
                HttpStatusCode.valueOf(500));
    }
    @Test
    void cancel_WhenSuccess_ShouldReturnSuccessMessage() {
        ParkingReservationDTO currentReservationDTO = currentReservationDTO();
        ParkingReservation currentReservation = currentReservation();

        when(modelMapper.map(currentReservationDTO, ParkingReservation.class)).thenReturn(currentReservation);
        when(currentReservationService.cancel(currentReservation)).thenReturn(Constants.CANCELLED_SUCCESS_MESSAGE);

        assertTrue(currentReservationController.cancel(currentReservationDTO).getBody().toString()
                .contains(Constants.CANCELLED_SUCCESS_MESSAGE));
    }

    @Test
    void cancel_WhenExceptionThrown_ShouldReturnInternalServerError() {
        ParkingReservationDTO currentReservationDTO = currentReservationDTO();
        ParkingReservation currentReservation = currentReservation();

        when(modelMapper.map(currentReservationDTO, ParkingReservation.class)).thenReturn(currentReservation);
        when(currentReservationService.cancel(currentReservation)).thenThrow(new RuntimeException());

        assertEquals(currentReservationController.cancel(currentReservationDTO).getStatusCode(),
                HttpStatusCode.valueOf(500));
    }

    private ParkingReservationDTO currentReservationDTO() {
        return new ParkingReservationDTO(1L, new UserDTO(),
                new ParkingSlotDTO(), LocalDateTime.now(), LocalDateTime.now().plusHours(2L),
                LocalDateTime.now(), null);
    }

    private ParkingReservation currentReservation() {
        return  new ParkingReservation(1L, new User(),
                new ParkingSlot(), LocalDateTime.now(), LocalDateTime.now().plusHours(2L),
                LocalDateTime.now(), null);
    }
}
