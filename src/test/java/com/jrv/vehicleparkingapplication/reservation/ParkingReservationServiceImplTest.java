package com.jrv.vehicleparkingapplication.reservation;

import com.jrv.vehicleparkingapplication.current_reservation.CurrentReservationService;
import com.jrv.vehicleparkingapplication.security.User;
import com.jrv.vehicleparkingapplication.security.UserRepository;
import com.jrv.vehicleparkingapplication.utils.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ParkingReservationServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private ParkingReservationRepository parkingReservationRepository;
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private ParkingSlotRepository parkingSlotRepository;
    @Mock
    private CurrentReservationService currentReservationService;

    @InjectMocks
    private ParkingReservationServiceImpl parkingReservationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void reserveParkingSlot_Successful() {
        User user = new User();
        user.setId(1L);
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        when(currentReservationService.getCurrentReservation(1L)).thenReturn(Optional.empty());

        ParkingSlot parkingSlot = new ParkingSlot();
        when(parkingSlotRepository.findByType(any())).thenReturn(Collections.singletonList(parkingSlot));

        when(modelMapper.map(any(), eq(ParkingReservation.class))).thenReturn(new ParkingReservation());

        when(parkingReservationRepository.save(any())).thenReturn(new ParkingReservation());

        ParkingReservationRequest request = new ParkingReservationRequest();
        request.setUserId(1L);
        request.setFromTime("2024-03-15T16:00:00.000Z");
        request.setToTime("2024-03-15T19:00:00.000Z");
        request.setMode("2W");
        ParkingReservationResponse response = parkingReservationService.reserveParkingSlot(request);

        assertNotNull(response);
        assertEquals(Constants.PARKING_RESERVED_SUCCESSFULLY, response.getMessage());
    }

    @Test
    void reserveParkingSlot_UserNotFound() {
        when(userRepository.findById(any())).thenReturn(Optional.empty());

        ParkingReservationRequest request = new ParkingReservationRequest();
        request.setUserId(1L);

        assertThrows(RuntimeException.class, () -> parkingReservationService.reserveParkingSlot(request));
    }

    @Test
    void reserveParkingSlot_UserAlreadyHasReservation() {
        User user = new User("name", "name@mail.com", "password");
        user.setId(1L);
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(currentReservationService.getCurrentReservation(anyLong())).thenReturn(Optional.of(new ParkingReservation()));

        List<ParkingSlot> allSlots = new ArrayList<>();
        allSlots.add(new ParkingSlot());
        when(parkingSlotRepository.findByType(any())).thenReturn(allSlots);
        when(modelMapper.map(any(), eq(ParkingReservation.class))).thenReturn(new ParkingReservation());

        ParkingReservationRequest request = new ParkingReservationRequest();
        request.setUserId(1L);
        request.setFromTime("2024-03-15T16:00:00.000Z");
        request.setToTime("2024-03-15T19:00:00.000Z");
        request.setMode("2W");
        assertThrows(UserAlreadyHasReservationException.class, () -> parkingReservationService.reserveParkingSlot(request));
    }

    @Test
    void reserveParkingSlot_NoParkingSlotsAvailable() {
        when(userRepository.findById(any())).thenReturn(Optional.of(new User()));
        when(currentReservationService.getCurrentReservation(anyLong())).thenReturn(Optional.empty());
        when(parkingSlotRepository.findByType(any())).thenReturn(Collections.emptyList());

        ParkingReservationRequest request = new ParkingReservationRequest();
        request.setUserId(1L);
        request.setFromTime("2024-03-15T16:00:00.000Z");
        request.setToTime("2024-03-15T19:00:00.000Z");
        request.setMode("2W");

        assertThrows(NoParkingSlotsAvailableException.class, () -> parkingReservationService.reserveParkingSlot(request));
    }

}
