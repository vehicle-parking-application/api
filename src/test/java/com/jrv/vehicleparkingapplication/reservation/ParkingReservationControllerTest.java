package com.jrv.vehicleparkingapplication.reservation;

import com.jrv.vehicleparkingapplication.utils.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ParkingReservationControllerTest {

    @Mock
    private ParkingReservationService parkingReservationService;

    @InjectMocks
    private ParkingReservationController parkingReservationController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void reserveParkingSlot_Successful() {
        ParkingReservationResponse expectedResponse = new ParkingReservationResponse(Constants.PARKING_RESERVED_SUCCESSFULLY);
        when(parkingReservationService.reserveParkingSlot(any())).thenReturn(expectedResponse);

        ParkingReservationRequest request = new ParkingReservationRequest();
        ResponseEntity<?> responseEntity = parkingReservationController.reserveParkingSlot(request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @ParameterizedTest
    @EnumSource(value = ParkingReservationExceptionEnum.class)
    void reserveParkingSlot_Exception(ParkingReservationExceptionEnum exceptionEnum) {
        String expectedMessage = exceptionEnum.getErrorMessage();
        HttpStatus expectedStatus = exceptionEnum.getHttpStatus();
        when(parkingReservationService.reserveParkingSlot(any()))
                .thenThrow(exceptionEnum.getException());

        ParkingReservationRequest request = new ParkingReservationRequest();
        ResponseEntity<?> responseEntity = parkingReservationController.reserveParkingSlot(request);

        assertEquals(expectedStatus, responseEntity.getStatusCode());
        assertEquals(expectedMessage, responseEntity.getBody());
    }

    enum ParkingReservationExceptionEnum {
        NO_SLOTS_AVAILABLE(Constants.NO_PARKING_SLOTS_AVAILABLE, HttpStatus.NOT_FOUND, new NoParkingSlotsAvailableException(Constants.NO_PARKING_SLOTS_AVAILABLE)),
        SLOT_ALREADY_RESERVED(Constants.USER_ALREADY_HAS_SLOT, HttpStatus.CONFLICT, new UserAlreadyHasReservationException(Constants.USER_ALREADY_HAS_SLOT)),
        HAS_EXCEPTION(Constants.ERROR, HttpStatus.INTERNAL_SERVER_ERROR, new RuntimeException(Constants.ERROR));

        private final String errorMessage;
        private final HttpStatus httpStatus;
        private final RuntimeException exception;

        ParkingReservationExceptionEnum(String errorMessage, HttpStatus httpStatus, RuntimeException exception) {
            this.errorMessage = errorMessage;
            this.httpStatus = httpStatus;
            this.exception = exception;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public HttpStatus getHttpStatus() {
            return httpStatus;
        }

        public RuntimeException getException() {
            return exception;
        }
    }
}
