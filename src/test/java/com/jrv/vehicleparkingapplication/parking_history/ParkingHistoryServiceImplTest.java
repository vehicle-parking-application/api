package com.jrv.vehicleparkingapplication.parking_history;

import com.jrv.vehicleparkingapplication.dto.ParkingReservationDTO;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservation;
import com.jrv.vehicleparkingapplication.reservation.ParkingReservationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class ParkingHistoryServiceImplTest {
    @Mock
    private ParkingReservationRepository parkingReservationRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private ParkingHistoryServiceImpl parkingHistoryService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getBriefHistory_WhenSuccess_ShouldReturnList(){
        List<ParkingReservationDTO> parkingReservationHistory = new ArrayList<>();
        ParkingReservationDTO parkingReservationDTO = new ParkingReservationDTO();
        parkingReservationHistory.add(parkingReservationDTO);

        List<ParkingReservation> parkingReservationList = new ArrayList<>();
        ParkingReservation parkingReservation = new ParkingReservation();
        parkingReservationList.add(parkingReservation);

        when(modelMapper.map(parkingReservation, ParkingReservationDTO.class)).thenReturn(parkingReservationDTO);

        when(parkingReservationRepository.findLatest10ByUserId(1L)).thenReturn(parkingReservationList);

        assertEquals(parkingHistoryService.getBriefHistory(1L).size(), 1);

    }
}
