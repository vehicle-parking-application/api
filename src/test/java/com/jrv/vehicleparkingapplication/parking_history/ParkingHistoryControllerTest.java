package com.jrv.vehicleparkingapplication.parking_history;

import com.jrv.vehicleparkingapplication.dto.ParkingReservationDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ParkingHistoryControllerTest {
    @Mock
    private ParkingHistoryService parkingHistoryService;

    @InjectMocks
    private ParkingHistoryController parkingHistoryController;

    @Test
    void getBriefHistory_WhenSuccess_ShouldReturnListOfParkingReservationHistory(){
        List<ParkingReservationDTO> parkingReservationHistory = new ArrayList<>();
        parkingReservationHistory.add(new ParkingReservationDTO());

        when(parkingHistoryService.getBriefHistory(1L)).thenReturn(parkingReservationHistory);

        ResponseEntity<?> responseEntity = parkingHistoryController.getBriefHistory(1L);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.getBody().equals(parkingReservationHistory));
    }

    @Test
    void getBriefHistory_WhenExceptionThrown_ShouldReturnInternalServerError(){
        when(parkingHistoryService.getBriefHistory(1L)).thenThrow(new RuntimeException());

        ResponseEntity<?> responseEntity = parkingHistoryController.getBriefHistory(1L);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }
}
